<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDateColumnsOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->timestamp('digital_start')->nullable()->after('polishing_sort');
            $table->timestamp('digital_complete')->nullable()->after('digital_start');
            $table->timestamp('polishing_start')->nullable()->after('digital_complete');
            $table->timestamp('polishing_complete')->nullable()->after('polishing_start');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('digital_start');
            $table->dropColumn('digital_complete');
            $table->dropColumn('polishing_start');
            $table->dropColumn('polishing_complete');
        });
    }
}
