<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string("order_unique_id", 100)->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string("design_number", 255)->nullable();
            $table->integer("quantity")->nullable();
            $table->integer("completed_quantity")->nullable();
            $table->enum('priority', array('None', 'Low', 'Medium', 'High'))->default('None');
            $table->longText('note')->nullable();

            $table->tinyInteger('admin_status')->comment('1:Pending, 2:Approve, 3:Reject')->default(1);

            $table->tinyInteger('digital_status')->comment('1:None, 2:Start, 3:Pause, 4:Stop, 5:Completed')->default(1);

            $table->tinyInteger('polishing_status')->comment('1:None, 2:Start, 3:Pause, 4:Stop, 5:Completed')->default(1);

            $table->tinyInteger('status')->comment('1:None, 2:Dispatch, 3:Delivered')->default(1);

            $table->integer('sort')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
