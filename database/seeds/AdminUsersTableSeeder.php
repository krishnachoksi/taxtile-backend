<?php

use Illuminate\Database\Seeder;

class AdminUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement("SET FOREIGN_KEY_CHECKS=0");

        // \DB::table('users')->truncate();

        \DB::table('users')->insert(array(
            0 =>
            array(
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'password' => bcrypt('12345678'),
                'user_type' => 'admin',
                'created_at' => '2018-04-01 17:04:00',
                'updated_at' => '2018-04-01 17:04:00',
                'deleted_at' => NULL,
            )
        ));

        \DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
