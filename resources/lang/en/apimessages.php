<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Webservice Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during rendering application for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'default_error_msg' => 'Whoops! Something went wrong. Please try again later.',
    'default_success_msg' => 'Success',
    'empty_data_msg' => 'Data Not Found',
    'norecordsfound' => 'No Records Found',
    'parameter_missing' => 'Parameter Missing',
    'unauthorized_access' => 'You are not authorized for this action',
    'image_updated_successfully' => 'Image updated successfully',
    'record_not_found' => 'This Record is not found',
    //login and registration api
    'user_not_found' => 'Account with this name does not exist.',
    'invalid_parameter' => 'Invalid request parameter.',
    'invalid_login' => 'Invalid Credentials',
    'logged_in' => 'You have logged in successfully.',
    'logout' => 'You have logged out successfully.',
    'error_in_register' => 'We encountered an error while registering this user.',
    'token_failed' => 'Failed to create token.',
    'email_already_exist' => 'User with same email already exist.',
    'user_created' => 'User created successfully.',
    'user_data_notfound' => 'User data not found.',
    'profile_update' => 'Profile picture update sucessfully.',
    'error_profile_update' => 'Error in profile update',
    'mail_sent' => 'Mail sent in your account.',
    'passwod_not_matched' => 'Your current password does not match with password you provided.',
    'can_not_be_same_as_old' => 'New password cannot be same as your old password.',
    'dashboard_count' => 'Total Number Of Count',
    //services
    'success_while_fetch_services' => 'Service list fetched successfully',
    'error_while_fetch_services' => 'Error while fetching services',
    'success_while_place_order' => 'Order placed successfully',
    'error_while_place_order' => 'Error while place order',
    'success_order_status' => 'Order details fetched successfully',
    'error_order_status' => 'Error while fetching order status',

    'order_list' => 'Order list',
    'error_order_list' => 'Error while fetching order list',
    'order_status_change' => 'Order status change successfully',
    'order_priority_change' => 'Order priority change successfully',
    'error_order_status_change' => 'Error while changing order status',
    'error_order_priority_change' => 'Error while changing order priority',
    'quantity_save_success' => 'Remaining quantity save successfully',
    'error_quantity_change' => 'Error while saving completed quantity',
    'order_delete' => 'Order deleted successfully',
    'error_order_delete' => 'Error while deleting order',

    'member_list' => 'Member list',
    'member_delete' => 'Member deleted successfully',
    'error_member_delete' => 'Error while deleting member',

    'product_list' => 'Product list',
    'success_while_add_product' => 'Product details saved successfully',
    'error_while_add_product' => 'Error while adding product',
    'product_delete' => 'Product deleted successfully',
    'error_product_delete' => 'Error while deleting product',

    'password_change_success' => 'Password change successfully.',
    'password_can_not_change' => 'Error while password change.',
    'user_save_success' => 'User detail save successfully.',
    'request_sent' => 'Your request sent successfully.',
    'error_request_sent' => 'Error while sending request',

    'success_while_adding_member' => 'Member details saved successfully',
    'error_while_adding_member' => 'Error while saving member details member'
];

