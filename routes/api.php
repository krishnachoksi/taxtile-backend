<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization, mimetype, Platform');

Route::post('auth/token', 'Api\AuthController@getToken');
Route::post('forgot-password', 'Api\UsersController@forgotPassword');

Route::group(['middleware' => 'jwt-auth'], function () 
{
	Route::post('logout', 'Api\AuthController@logout');

	//General routes
	Route::post('changePassword', 'Api\UsersController@changePassword');
	Route::post('updateProfiePicture', 'Api\UsersController@updateProfilePicture');
	Route::post('updateProfile', 'Api\UsersController@updateProfile');

	Route::get('getCount', 'Api\UsersController@getAllCount');

	//Order api routes
	Route::get('getOrders', 'Api\OrderController@getAllOrders');
	Route::post('saveOrder', 'Api\OrderController@saveOrder');
	Route::post('changeOrderStatusById', 'Api\OrderController@changeOrderStatusById');
	Route::get('getOrderByUserId', 'Api\OrderController@getOrderByUserId');
	Route::post('swapOrder', 'Api\OrderController@swapOrder');
	Route::post('swapPolishingOrder', 'Api\OrderController@swapPolishingOrder');
	Route::post('completedQuantity', 'Api\OrderController@completedQuantity');
	Route::post('deleteOrder', 'Api\OrderController@deleteOrder');

	//Product api routes
	Route::get('getProducts', 'Api\ProductController@getAllProducts');
	Route::post('saveProduct', 'Api\ProductController@saveProduct');
	Route::post('deleteProduct', 'Api\ProductController@deleteProduct');
	Route::get('getProductByUserId', 'Api\ProductController@getProductByUserId');

	//Members api routes
	Route::get('getMembers', 'Api\UsersController@getAllMembers');
	Route::post('saveMember', 'Api\UsersController@saveMember');
	Route::post('deleteMember', 'Api\UsersController@deleteMember');

	
});
