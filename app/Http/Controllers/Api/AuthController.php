<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use JWTAuthException;
use Config;
use App\User;
use Validator;
use DB;

class AuthController extends Controller {

    public function __construct() 
    {
        $this->objUser = new User();
    }

    //login api
    public function getToken(Request $request) {

        $validator = Validator::make($request->all(), [
                    'email' => 'required',
                    'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                        'status' => 0,
                        'message' => $validator->messages()->all()[0]
            ], 400);
        }
        
        $token = null;
        
        $userData = User::where('name', $request->email)->first();
    
        if($userData === null) {
            return response()->json([
                        'status' => 0,
                        'message' => trans('apimessages.user_not_found')
            ], 404);
        }
        
        //$credentials = $request->only('email', 'password');
        $credentials = [
            'name' => $request->email,
            'password' => $request->password
        ];
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                            'status' => 0,
                            'message' => trans('apimessages.invalid_login')
                ], 400);
            }
        } catch (JWTAuthException $e) {
            return response()->json([
                        'status' => 0,
                        'message' => trans('apimessages.default_error_msg')
                        
            ], 500);
        }
        
        return response()->json([
                    'status' => 1,
                    'message' => trans('apimessages.logged_in'),
                    'data' => [
                        'userDetail' => $request->user(),
                        'loginToken' => compact('token')
                    ]
        ]);
    }
    
    // Logout APi
     
    public function logout(Request $request) 
    {
        try 
        {
            $user = JWTAuth::parseToken()->authenticate();
            DB::beginTransaction(); 

            JWTAuth::invalidate(JWTAuth::getToken());
            DB::commit();
            $outputArray['status'] = 1;
            $outputArray['message'] = trans('apimessages.logout');
            $statusCode = 200;
            $outputArray['data'] = [];
            return response()->json($outputArray, $statusCode);
        } catch (Exception $e) 
        {
            DB::rollback();
            $outputArray['status'] = 0;
            $outputArray['message'] = trans('apimessages.default_error_msg');
            $statusCode =  $e->getStatusCode();
            return response()->json($outputArray, $statusCode);
        }
    }
}