<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Support;
use App\User;
use App\UserBalance;
use Illuminate\Validation\Rule;
use Auth;
use JWTAuth;
use JWTAuthException;
use DB;
use Validator;
use Config;
use Input;
use Image;
use File;
use Mail;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class SupportController extends Controller {

    private $user;

    public function __construct() {
        $this->support = new Support();
        $this->userThumbImageUploadPath = Config::get('constant.USER_THUMB_IMAGE_UPLOAD_PATH');
        $this->userOriginalImageUploadPath = Config::get('constant.USER_ORIGINAL_IMAGE_UPLOAD_PATH');
        $this->userThumbImageHeight = Config::get('constant.USER_THUMB_IMAGE_HEIGHT');
        $this->userThumbImageWidth = Config::get('constant.USER_THUMB_IMAGE_WIDTH');
    }
    //Send Messages
    public function sendMessages(Request $request) {

        try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();
            $validator = Validator::make($request->all(), [
                'message' => 'required'
            ]);

            if ($validator->fails()) {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => $validator->messages()->all()[0],
                    'code' => '20100'
                ]);
            }

            $data = [];
            $data['message'] = $request->message;
            $data['user_id'] = $user->id;
            $data['sender_id'] = $user->id;
            $data['receiver_id'] = 1;

            $saveMessage = $this->support->saveMessage($data);
            if($saveMessage)
            {
                DB::commit();
                return response()->json([
                    'status' => '1',
                    'message' => trans('apimessages.request_sent'),
                    'data' => $saveMessage
                ]);
            }else
            {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_request_sent')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.default_error_msg'),
                        'code' => $e->getStatusCode()
            ]);
        }
    }
    //Get Messages
    public function getMessages(Request $request) {

        try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();

            $getMessages = $this->support->getMessage($user->id);
            $user->profile_pic = ($user->profile_pic != NULL && $user->profile_pic != '') ? url($this->userThumbImageUploadPath.$user->profile_pic) : '';
            $adminDetail = User::where('is_admin',1)->first();
            $adminDetail->profile_pic = asset('css/admin/dist/img/'.$adminDetail->profile_pic);
            if($getMessages)
            {
                DB::commit();
                return response()->json([
                    'status' => '1',
                    'message' => trans('apimessages.request_sent'),
                    'data' => [
                        "message" => $getMessages,
                        "userDetail" => $user,
                        "adminDetail" => $adminDetail
                    ]
                ]);
            }else
            {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_request_sent')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.default_error_msg'),
                        'code' => $e->getStatusCode()
            ]);
        }
    }

    public function changeMessageStatus(){
        try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();

            $statusChange = $this->support->where('user_id', Input::get('user_id'))->where('receiver_id', Input::get('user_id'))->update(['read_status'=>1]);
            $token = Input::get('token');

            $transaction = UserBalance::where('user_id',$user->id)->get();
            $messageCount = Support::where('user_id',$user->id)->where('receiver_id',$user->id)->where('read_status',0)->count();

            if($statusChange)
            {
                DB::commit();
                return response()->json([
                    'status' => '1',
                    'message' => trans('apimessages.request_sent'),
                    'data' => [
                        'userDetail'=> $user,
                        'transactionHistory' => $transaction,
                        'loginToken' => compact('token'),
                        'messageCount' => $messageCount
                    ]
                ]);
            }else
            {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_request_sent')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.default_error_msg'),
                        'code' => $e->getStatusCode()
            ]);
        }
    }
}