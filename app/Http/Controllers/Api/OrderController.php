<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Orders;
use Illuminate\Validation\Rule;
use Auth;
use JWTAuth;
use JWTAuthException;
use DB;
use Validator;
use Config;
use Input;
use Helpers;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class OrderController extends Controller {

    private $user;

    public function __construct() {
        $this->orders = new Orders();
        $this->userThumbImageUploadPath = Config::get('constant.USER_THUMB_IMAGE_UPLOAD_PATH');
        $this->userOriginalImageUploadPath = Config::get('constant.USER_ORIGINAL_IMAGE_UPLOAD_PATH');
        $this->userThumbImageHeight = Config::get('constant.USER_THUMB_IMAGE_HEIGHT');
        $this->userThumbImageWidth = Config::get('constant.USER_THUMB_IMAGE_WIDTH');
    }
    
    //Get All Orders
    public function getAllOrders() {

       try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();

            $getOrderData = $this->orders->orderBy('sort','asc')->get();
            //$getOrderData = $this->orders->groupBy('order_unique_id')->orderBy('sort','asc')->get(['id','sort','completed_quantity','polishing_sort' ,'polishing_completed_quantity','polishing_status','digital_status','design_number','user_id','note','created_at','updated_at','deleted_at','status','priority',DB::raw('MAX(quantity) as quantity')]);

            $getPolishingOrderData = $this->orders->where('digital_status',5)->orderBy('polishing_sort','asc')->get();

            $getPendingOrderData = $this->orders->join('users', 'users.id', '=', 'orders.user_id')->where('orders.admin_status', 1)->get(['orders.id as id','orders.user_id','orders.status','orders.admin_status','orders.digital_status','orders.polishing_status','orders.design_number','orders.quantity','orders.completed_quantity','orders.note','orders.sort','orders.created_at','orders.updated_at','orders.deleted_at','orders.priority','users.name','users.company_name']);

            $getApprovedOrderData = $this->orders->join('users', 'users.id', '=', 'orders.user_id')->where('orders.admin_status', 2)->where('orders.digital_status', '<>', 5)->where('orders.polishing_status','<>', 5)->groupBy('orders.order_unique_id')->orderBy('orders.sort','asc')->get(['orders.id as id','orders.user_id','orders.admin_status','orders.status','orders.digital_status','orders.polishing_status','orders.design_number','orders.completed_quantity','orders.note','orders.sort','orders.priority','orders.created_at','orders.updated_at','orders.deleted_at','users.name','users.company_name',DB::raw('MAX(quantity) as quantity')]);

            $getRejectOrderData = $this->orders->join('users', 'users.id', '=', 'orders.user_id')->where('admin_status', 3)->get(['orders.id as id','orders.user_id','orders.admin_status','orders.digital_status','orders.polishing_status','orders.design_number','orders.quantity','orders.completed_quantity','orders.note','orders.sort','orders.priority','orders.created_at','orders.updated_at','orders.deleted_at','users.name','users.company_name']);

            //digital user count
            $digitalPending = $this->orders->where('digital_status', 1)->where('admin_status',2)->count();
            $digitalStartPause = $this->orders->whereIn('digital_status', [2,3])->where('admin_status',2)->count();
            
            //polishing user count
            $polishingPending = $this->orders->where('polishing_status', 1)->where('digital_status',5)->count();
            $polishingStartPause = $this->orders->whereIn('polishing_status', [2,3])->where('digital_status',5)->count();
            $completed = $this->orders->where('polishing_status', 5)->where('digital_status', 5)->count();

            if($getOrderData != '' && isset($getOrderData)){
                foreach ($getOrderData as $key => $value) {
                    $userData = User::where('id',$value->user_id)->first();
                    if($userData){
                        $getOrderData[$key]['company_name'] = $userData->company_name;
                        $getOrderData[$key]['name'] = $userData->name;
                    }
                }  
            }
            if($getOrderData)
            {
                DB::commit();
                return response()->json([
                    'status' => '1',
                    'message' => trans('apimessages.order_list'),
                    'data' => $getOrderData,
                    'getPendingOrderData' => $getPendingOrderData,
                    'getApprovedOrderData' => $getApprovedOrderData,
                    'getRejectOrderData' => $getRejectOrderData,
                    'digitalPending' => $digitalPending,
                    'digitalStartPause' => $digitalStartPause,
                    'polishingPending' => $polishingPending,
                    'polishingStartPause' => $polishingStartPause,
                    'completed' => $completed,
                    'getPolishingOrderData' => $getPolishingOrderData
                ]);
            }else
            {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_order_list')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.default_error_msg'),
                        'code' => $e->getStatusCode()
            ]);
        }
    }

    //Save Order details
    public function saveOrder(Request $request) {
        try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();
            $validator = Validator::make($request->all(), [
                'note' => 'max:500',
                'design_number' => 'required',
                'quantity' => 'required|max:200'
            ]);
            
            if ($validator->fails()) {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => $validator->messages()->all()[0],
                    'code' => '20100'
                ]);
            }
            $requestData = $request->all();
            
            $requestData['user_id'] = $user->id;
            $sort = $this->orders->pluck('sort');
            
            if(count($sort) <= 0){
                $requestData['sort'] = 1;
            }else{
                $requestData['sort'] = $sort->last() + 1;
            }

            $polishing_sort = $this->orders->pluck('polishing_sort');
            
            if(count($polishing_sort) <= 0){
                $requestData['polishing_sort'] = 1;
            }else{
                $requestData['polishing_sort'] = $polishing_sort->last() + 1;
            }

            //generate unique order id
            $order_unique_id = str_random(6);
            $getOrderId = Orders::where('order_unique_id', $order_unique_id )->first();
            if(!empty($getOrderId))
            {
                $order_unique_id = str_random(6);
            }
            $requestData['order_unique_id'] = $order_unique_id;
            if($user->user_type == 'admin'){
                $requestData['admin_status'] = 2;
            }
            $orderDetails = $this->orders->saveOrderData($requestData); 

            if($orderDetails)
            {
                    DB::commit();
                    return response()->json([
                        'status' => '1',
                        'message' => trans('apimessages.success_while_place_order'),
                        'data' => $orderDetails
                    ]);
            }else
            {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_while_place_order')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.default_error_msg'),
                        'code' => $e->getStatusCode()
            ]);
        }
    }

    //Get order by user Id
    public function getOrderByUserId(Request $req) {
        try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();
            $completed = 0;
            $getOrderData = $this->orders->getOrderByUserId($user);
            //digital user count
            $digitalPending = $this->orders->where('digital_status', 1)->where('admin_status',2)->count();
            $digitalStartPause = $this->orders->whereIn('digital_status', [2,3])->where('admin_status',2)->count();
            
            //polishing user count
            $polishingPending = $this->orders->where('polishing_status', 1)->where('digital_status',5)->count();
            $polishingStartPause = $this->orders->whereIn('polishing_status', [2,3])->where('digital_status',5)->count();
            
            if($user->user_type == 'digitalmachine'){
                $completed = $this->orders->where('digital_status', 5)->count();
            }
            if($user->user_type == 'polishingmachine'){
                $completed = $this->orders->where('polishing_status', 5)->count();
            }

            // if($getOrderData != '' && isset($getOrderData)){
            //     foreach ($getOrderData as $key => $value) {
            //        print_r($getOrderData[0]->user_id);die;
            //         $userData = User::where('id',$value->user_id)->first();
            //         if($userData){
            //             $getOrderData[$key]['company_name'] = $userData->company_name;
            //         }
            //     }  
            // }
            //print_r($getOrderData);die;
            $pendingOrderDetails = Orders::where('user_id', $user['id'])->where('admin_status', 1)->orderBy('sort','asc')->get();
            $approvedOrderDetails = Orders::where('user_id', $user['id'])->where('admin_status', 2)->orderBy('sort','asc')->get();
            $rejectedOrderDetails = Orders::where('user_id', $user['id'])->where('admin_status', 3)->orderBy('sort','asc')->get();

            if($getOrderData)
            {
                DB::commit();
                return response()->json([
                    'status' => '1',
                    'message' => trans('apimessages.order_list'),
                    'data' => $getOrderData,
                    'pendingOrderDetails' => $pendingOrderDetails,
                    'approvedOrderDetails' => $approvedOrderDetails,
                    'rejectedOrderDetails' => $rejectedOrderDetails,
                    'digitalPending' => $digitalPending,
                    'digitalStartPause' => $digitalStartPause,
                    'polishingPending' => $polishingPending,
                    'polishingStartPause' => $polishingStartPause,
                    'completed' => $completed
                ]);
            }else
            {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_order_list')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.default_error_msg'),
                        'code' => $e->getStatusCode()
            ]);
        }
    }

    //Change order status by Order Id
    public function changeOrderStatusById(Request $request) {
        try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();

            $validator = Validator::make($request->all(), [
                'order_id' => ['required', 'max:500'],
                'status' => 'required'
            ]);

            if ($validator->fails()) {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => $validator->messages()->all()[0],
                    'code' => '20100'
                ]);
            }

            $requestData = $request->all();
            $changeOrderStatus = $this->orders->changeOrderStatus($requestData,$user);
            if($changeOrderStatus)
            {
                DB::commit();
                return response()->json([
                    'status' => '1',
                    'message' => trans('apimessages.order_status_change')
                ]);
            }else
            {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_order_status_change')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.default_error_msg'),
                        'code' => $e->getStatusCode()
            ]);
        }
    }

    //swaping order
    public function swapOrder(Request $request) {
        try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();
            $requestData = $request->all();
            $sort = 1;
            foreach ($requestData as $key => $value) {
                $update = Orders::where('id',$value)->update(['sort'=> $sort]);
                $sort++;
            }
            if($update)
            {
                DB::commit();
                return response()->json([
                    'status' => '1',
                    'message' => trans('apimessages.order_priority_change')
                ]);
            }else
            {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_order_priority_change')
                ]);
            }
        }catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.default_error_msg'),
                        'code' => $e->getStatusCode()
            ]);
        }
    }

    //swaping polishing order
    public function swapPolishingOrder(Request $request) {
        try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();
            $requestData = $request->all();
            $sort = 1;
            foreach ($requestData as $key => $value) {
                $update = Orders::where('id',$value)->update(['polishing_sort'=> $sort]);
                $sort++;
            }
            if($update)
            {
                DB::commit();
                return response()->json([
                    'status' => '1',
                    'message' => trans('apimessages.order_priority_change')
                ]);
            }else
            {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_order_priority_change')
                ]);
            }
        }catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.default_error_msg'),
                        'code' => $e->getStatusCode()
            ]);
        }
    }
    
    public function completedQuantity(Request $request){
        try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();
            $validator = Validator::make($request->all(), [
                'quantity' => 'required',
            ]);

            if ($validator->fails()) {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => $validator->messages()->all()[0],
                    'code' => '20100'
                ]);
            }

            $requestData = $request->all();
            $saveQuantity = $this->orders->saveQuantity($requestData,$user);
           // print_r($saveQuantity);die;
            
            if($saveQuantity){
                DB::commit();
                $order = Orders::find($requestData['order_id']);
                //print_r($order);die;
                return response()->json([
                    'status' => '1',
                    'message' => trans('apimessages.quantity_save_success'),
                    'data' => $order
                ]);
            }else{
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_quantity_change')
                ]);
            }
           

        }catch (Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => '0',
                'message' => trans('apimessages.default_error_msg'),
                'code' => $e->getStatusCode()
            ]);
        }
    }  

    //Delete order
    public function deleteOrder(Request $request) {

       try {
            DB::beginTransaction();
            $id = $request->id;
            $deleteOrder = $this->orders->deleteOrder($id);
            if($deleteOrder)
            {
                DB::commit();
                return response()->json([
                    'status' => '1',
                    'message' => trans('apimessages.order_delete'),
                    'data' => $deleteOrder
                ]);
            }else
            {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_order_delete')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.default_error_msg'),
                        'code' => $e->getStatusCode()
            ]);
        }
    } 
}