<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Orders;
use Illuminate\Validation\Rule;
use Auth;
use JWTAuth;
use JWTAuthException;
use DB;
use Validator;
use Config;
use Input;
use Image;
use File;
use Mail;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UsersController extends Controller {

    private $user;

    public function __construct() {
        $this->user = new User();
        $this->userThumbImageUploadPath = Config::get('constant.USER_THUMB_IMAGE_UPLOAD_PATH');
        $this->userOriginalImageUploadPath = Config::get('constant.USER_ORIGINAL_IMAGE_UPLOAD_PATH');
        $this->userThumbImageHeight = Config::get('constant.USER_THUMB_IMAGE_HEIGHT');
        $this->userThumbImageWidth = Config::get('constant.USER_THUMB_IMAGE_WIDTH');
    }
    
    //Add New Member
    public function saveMember(Request $request) {
        try {
            DB::beginTransaction();
            // $user = JWTAuth::parseToken()->authenticate();
            if($request->id == ''){
                $validator = Validator::make($request->all(), [
                    'name' => ['required', 'max:50', Rule::unique('users', 'name')->ignore($request->id), 'regex:/^[a-zA-Z-_\.\/ ]+$/'],
                    // 'email' => ['required', 'email', 'max:40',Rule::unique('users', 'email')->ignore($request->id)],
                    'password' => 'required|min:8|max:20',
                    // 'company_name' => 'required',
                    'user_type' => 'required'
                ]);
            }else{
                $validator = Validator::make($request->all(), [
                    'name' => ['required', 'max:50', Rule::unique('users', 'name')->ignore($request->id), 'regex:/^[a-zA-Z-_\.\/ ]+$/'],
                    // 'email' => ['required', 'email', 'max:40',Rule::unique('users', 'email')->ignore($request->id)],
                    // 'company_name' => 'required',
                    'user_type' => 'required'
                ]);
            }
            if ($validator->fails()) {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => $validator->messages()->all()[0],
                    'code' => '20100'
                ]);
            }
            $requestData = $request->all();
            $memberDetails = $this->user->saveMemberData($requestData); 
            //_r($memberDetails);die;
            if($memberDetails)
            {
                    DB::commit();
                    return response()->json([
                        'status' => '1',
                        'message' => trans('apimessages.success_while_adding_member'),
                        'data' => $memberDetails
                    ]);
            }else
            {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_while_adding_member')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.default_error_msg'),
                        'code' => $e->getStatusCode()
            ]);
        }
    }

    //Get All Members
    public function getAllMembers() {

       try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();

            $getMemberData = $this->user->getAllMembers();
            if($getMemberData)
            {
                DB::commit();
                return response()->json([
                    'status' => '1',
                    'message' => trans('apimessages.member_list'),
                    'data' => $getMemberData
                ]);
            }else
            {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_member_list')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.default_error_msg'),
                        'code' => $e->getStatusCode()
            ]);
        }
    }

    //Delete Members
    public function deleteMember(Request $request) {

       try {
            DB::beginTransaction();
            $id = $request->id;
            
            $expId = explode(',',$id);
            foreach ($expId as $key => $value) {
                //$deleteMember = $this->user->deleteMember($value);
                Orders::where('user_id', $value)->forceDelete();
                $user = User::find($value);
                $deleteMember = $user->forceDelete();
            }
            
            if($deleteMember)
            {
                DB::commit();
                return response()->json([
                    'status' => '1',
                    'message' => trans('apimessages.member_delete'),
                    'data' => $deleteMember
                ]);
            }else
            {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_member_delete')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.default_error_msg'),
                        'code' => $e->getStatusCode()
            ]);
        }
    }

    //get count
    public function getAllCount() {

       try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();
        
            $getCountData = $this->user->getAllCount($user);
            if($getCountData)
            {
                DB::commit();
                return response()->json([
                    'status' => '1',
                    'message' => trans('apimessages.dashboard_count'),
                    'data' => $getCountData
                ]);
            }else
            {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_member_list')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.default_error_msg'),
                        'code' => $e->getStatusCode()
            ]);
        }
    }

    //Update Profile Picture
    public function updateProfilePicture(Request $request) {
        
        try {
            $validator = Validator::make($request->all(), [
                'profile_pic' => 'required|image|mimes:jpeg,png,jpg|max:5120'
            ]);

            if ($validator->fails()) {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => $validator->messages()->all()[0],
                    'code' => '20100'
                ]);
            }
            DB::beginTransaction();
            //get user data
            $userData = User::find($request->user()->id);
            if ($userData === null) {
                DB::rollback();
                return response()->json([
                            'status' => '0',
                            'message' => trans('apimessages.user_data_notfound'),
                            'code' => 404
                ]);
            }
            $fileName = 'user_pic'. str_random(20) .'.'.$request->profile_pic->getClientOriginalExtension();

            $pathOriginal = public_path($this->userOriginalImageUploadPath . $fileName);
            $pathThumb = public_path($this->userThumbImageUploadPath . $fileName);

            if (!file_exists(public_path($this->userOriginalImageUploadPath))) File::makeDirectory(public_path($this->userOriginalImageUploadPath), 0777, true, true);
            if (!file_exists(public_path($this->userThumbImageUploadPath))) File::makeDirectory(public_path($this->userThumbImageUploadPath), 0777, true, true);

            // created instance
            $img = Image::make($request->profile_pic->getRealPath());

            $img->save($pathOriginal);
            // resize the image to a height of $this->userThumbImageHeight and constrain aspect ratio (auto width)
            if( $img->height() < 500 ){
                $img->resize(null, $img->height(), function ($constraint) {
                    $constraint->aspectRatio();
                })->save($pathThumb);
            } else {
                $img->resize(null, $this->userThumbImageHeight, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($pathThumb);
            }
            
            $userData->profile_pic = $fileName;
            $userData->save();

            DB::commit();
            return response()->json([
                'status' => '1',
                'message' => trans('apimessages.profile_update'),
            ]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.error_profile_update'),
                        'code' => $e->getStatusCode()
            ]);
        }
    }
    
    // Forgot password 
    public function forgotPassword(Request $request) {
        try {
            DB::beginTransaction();
            $validator = Validator::make($request->all(), [
                'forgot_email' => ['required', 'email', 'max:255'],
            ]);

            if ($validator->fails()) {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => $validator->messages()->all()[0],
                    'code' => '20100'
                ]);
            }
            // Get user detail from database
            $user = User::where('email', $request->forgot_email)->first();

            // User not exist
            if (is_null($user)) {
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.user_data_notfound'),
                    'code' => '20100'
                ]);
            }

            DB::table('password_resets')->where('email', $request->forgot_email)->delete();

            //create a new token to be sent to the user. 
            DB::table('password_resets')->insert([
                'email' => $request->forgot_email,
                'token' => str_random(80),
                'created_at' => Carbon::now()
            ]);

            $tokenData = DB::table('password_resets')->where('email', $request->forgot_email)->first();

            $token = $tokenData->token;
            $email = $request->forgot_email;

            // Send Password reset mail
            $data = [
                'url' => url('password/reset/' . $token),
                'username' => ($user->name != null && isset($user->name))?$user->name:""
            ];

            Mail::send('emails.ResetPassword', $data, function($message) use($email) {
                $message->to($email)->subject('Your Password Reset Link');
            });

            DB::commit();
            return response()->json([
                'status' => '1',
                'message' => trans('apimessages.mail_sent'),
                'data' => ""
            ]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => '0',
                'message' => 'error',
                'code' => $e->getStatusCode()
            ]);
        }
    }
   
    //change password
    public function changePassword(Request $request){
        try {
            DB::beginTransaction();

            $validator = Validator::make($request->all(), [
                'old_password' => 'required',
                'new_password' => 'required',
                'password_confirmation'=>'same:new_password|required_with:new_password',
            ]);

            if ($validator->fails()) {
                DB::rollback();
                return response()->json([
                            'status' => '0',
                            'message' => $validator->messages()->all()[0],
                            'code' => '20100'
                ]);
            }

            $user = User::where('id', $request->user()->id)->first();
            if(!is_null($user)){
                if(Hash::check($request->old_password, $request->user()->password))
                {
                    $user->password = $request->new_password;
                    $user->save();
                }
                else{
                    DB::rollback();
                    return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.passwod_not_matched')
                    ]);
                }
                if(strcmp($request->old_password, $request->new_password) == 0){
                   DB::rollback();
                    return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.can_not_be_same_as_old')
                    ]); 
                }
                
            }else{
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.user_data_notfound')
                ]);
            }
            JWTAuth::invalidate(JWTAuth::getToken());
            DB::commit();
            return response()->json([
                'status' => '1',
                'message' => trans('apimessages.password_change_success')
            ]);

        }catch (Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => '0',
                'message' => trans('apimessages.password_can_not_change'),
                'code' => $e->getStatusCode()
            ]);
        }
    }

    public function updateProfile(Request $request){
        try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'max:50', 'regex:/^[a-zA-Z-_\.\/ ]+$/'],
                'email' => ['required', 'email', 'max:40',Rule::unique('users', 'email')->ignore($request->id)],
                'company_name' => 'required',
            ]);

            if ($validator->fails()) {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => $validator->messages()->all()[0],
                    'code' => '20100'
                ]);
            }

            //user same email check
            $userData = User::where('id','!=',$user->id)->get();
            foreach ($userData as $key => $value) {
                
                if( $request->email == $value->email ) {
                    DB::rollback();
                    return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.email_already_exist'),
                        'code' => 400
                    ]);
                }
            }

            $data = $request->only('name', 'email','company_name');
            $token = $request->token;
            $user = User::find($user->id);
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->company_name = $data['company_name'];
            $user->save();

            DB::commit();
            return response()->json([
                'status' => '1',
                'message' => trans('apimessages.user_save_success'),
                'data' => [
                        'userDetail' => $user,
                        'loginToken' => compact('token')
                    ]
            ]);

        }catch (Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => '0',
                'message' => trans('apimessages.default_error_msg'),
                'code' => $e->getStatusCode()
            ]);
        }
    }
}
