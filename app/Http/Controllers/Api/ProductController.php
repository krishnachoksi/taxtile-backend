<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Products;
use Illuminate\Validation\Rule;
use Auth;
use JWTAuth;
use JWTAuthException;
use DB;
use Validator;
use Config;
use Input;
use Helpers;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class ProductController extends Controller {

    private $user;

    public function __construct() {
        $this->products = new Products();
        $this->userThumbImageUploadPath = Config::get('constant.USER_THUMB_IMAGE_UPLOAD_PATH');
        $this->userOriginalImageUploadPath = Config::get('constant.USER_ORIGINAL_IMAGE_UPLOAD_PATH');
        $this->userThumbImageHeight = Config::get('constant.USER_THUMB_IMAGE_HEIGHT');
        $this->userThumbImageWidth = Config::get('constant.USER_THUMB_IMAGE_WIDTH');
    }
    
    //Get All Products
    public function getAllProducts() {
       try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();

            $getProductData = $this->products->all();
            
            if($getProductData)
            {
                DB::commit();
                return response()->json([
                    'status' => '1',
                    'message' => trans('apimessages.product_list'),
                    'data' => $getProductData
                ]);
            }else
            {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_order_list')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.default_error_msg'),
                        'code' => $e->getStatusCode()
            ]);
        }
    }

    //Save Product
    public function saveProduct(Request $request) {
        try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();
            $requestData = $request->all();
            if(isset($requestData['id'])){
                $validator = Validator::make($request->all(), [
                    'design_number' => ['required','numeric', Rule::unique('products', 'design_number')->ignore($request->id)]
                    // 'product_name' => 'required',
                    // 'description' => 'max:500'
                ]);
            }else{
                $validator = Validator::make($request->all(), [
                    'design_number' => ['required', Rule::unique('products', 'design_number')->ignore($request->id)]
                    // 'product_name' => 'required',
                    // 'description' => 'max:500'
                ]);
            }
            if ($validator->fails()) {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => $validator->messages()->all()[0],
                    'code' => '20100'
                ]);
            }
            
            if(!isset($requestData['id']))
            {
                $response = [];
                //$requestData['design_number'] = explode(",", $requestData['design_number']);
                $requestData['design_number'] = preg_split("/[\,-]+/", $requestData['design_number']);
                //print_r($requestData['design_number']);die;
                if($requestData['design_number'])
                {
                    foreach ($requestData['design_number'] as $key => $value) {
                        $designNumber = Products::where('design_number',$value)->first();

                        if(!empty($designNumber))
                        {
                            return response()->json([
                                'status' => '0',
                                'message' => "Design Number ".$value." is already exist",
                                'code' => '20100'
                            ]);
                        }
                    }
                }
            }
            // $requestData['user_id'] = $user->id;
            $productDetails = $this->products->saveProductData($requestData); 

            if($productDetails)
            {
                    DB::commit();
                    return response()->json([
                        'status' => '1',
                        'message' => trans('apimessages.success_while_add_product'),
                        'data' => $productDetails
                    ]);
            }else
            {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_while_add_product')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.default_error_msg'),
                        'code' => $e->getStatusCode()
            ]);
        }
    }

    //Get product by user Id
    public function getProductByUserId(Request $req) {
        try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();

            $getOrderData = $this->orders->getOrderByUserId($user->id);
            if($getOrderData)
            {
                DB::commit();
                return response()->json([
                    'status' => '1',
                    'message' => trans('apimessages.order_list'),
                    'data' => $getOrderData
                ]);
            }else
            {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_order_list')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.default_error_msg'),
                        'code' => $e->getStatusCode()
            ]);
        }
    }

    //Delete product
    public function deleteProduct(Request $request) {

       try {
            DB::beginTransaction();
            $id = $request->id;
            $deleteProduct = $this->products->deleteProduct($id);
            if($deleteProduct)
            {
                DB::commit();
                return response()->json([
                    'status' => '1',
                    'message' => trans('apimessages.product_delete'),
                    'data' => $deleteProduct
                ]);
            }else
            {
                DB::rollback();
                return response()->json([
                    'status' => '0',
                    'message' => trans('apimessages.error_product_delete')
                ]);
            }
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                        'status' => '0',
                        'message' => trans('apimessages.default_error_msg'),
                        'code' => $e->getStatusCode()
            ]);
        }
    }
}