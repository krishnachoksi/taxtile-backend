<?php 
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use DB;

class Orders extends Model 
{
    use Notifiable;

    use SoftDeletes;

    protected $table = "orders";

    protected $guarded = [];
	// protected $fillable = [
	// 'title', 'photo','description','html','css'
	// ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    public function scopeSearchName($query, $value) {
        return $query->orWhere('users.name', 'LIKE', "%$value%");
    }

    public function scopeSearchOrderId($query, $value) {
        return $query->orWhere('order_id', 'LIKE', "%$value%");
    }

    public function scopeSearchServiceName($query, $value) {
        return $query->orWhere('service_details', 'LIKE', "%$value%");
    }

    public function saveOrderData($data)
    {
        return Orders::create($data);
    }

    public function getOrderByUserId($user)
    {
        if($user['user_type'] == 'marketing'){
            $orderDetails = Orders::where('user_id', $user['id'])->orderBy('sort','asc')->get();
        }
        if($user['user_type'] == 'digitalmachine'){
            $orderDetails['digital_pending'] = Orders::where('admin_status', 2)->where('digital_status',1)->orderBy('sort','asc')->get();
            $orderDetails['digital_start'] = Orders::where('admin_status', 2)->where('digital_status',2)->orWhere('digital_status',3)->orderBy('sort','asc')->get();
            $orderDetails['digital_completed'] = Orders::where('admin_status', 2)->where('digital_status',5)->orderBy('sort','asc')->get();
            $orderDetails['all'] = Orders::where('admin_status', 2)->orderBy('sort','asc')->get();
        }
        if($user['user_type'] == 'polishingmachine'){
            $orderDetails['polishing_pending'] = Orders::where('admin_status', 2)->where('polishing_status',1)->where('digital_status',5)->orderBy('sort','asc')->get();
            $orderDetails['polishing_start'] = Orders::where('admin_status', 2)->where('polishing_status',2)->where('digital_status',5)->orWhere('polishing_status',3)->orderBy('sort','asc')->get();
            $orderDetails['polishing_completed'] = Orders::where('admin_status', 2)->where('polishing_status',5)->where('digital_status',5)->orderBy('sort','asc')->get();
           $orderDetails['all'] = Orders::where('digital_status', 5)->orderBy('sort','asc')->get();
        }
        return $orderDetails;
    }

    public function changeOrderStatus($orderData, $userData)
    {
        if($userData['user_type'] == 'admin'){
            $orderStatus = Orders::where('id', $orderData['order_id'])->update(['admin_status' => $orderData['status']]);
        }
        if($userData['user_type'] == 'digitalmachine'){
            if($orderData['status'] == 2){
                $getOrderData = Orders::where('digital_status', 2)->update(['digital_status' => 5,'digital_complete' => DB::raw('CURRENT_TIMESTAMP')]);
                Orders::where('id', $orderData['order_id'])->update(['digital_start' => DB::raw('CURRENT_TIMESTAMP')]);
            }
            $orderStatus = Orders::where('id', $orderData['order_id'])->update(['digital_status' => $orderData['status']]);
        }
        if($userData['user_type'] == 'polishingmachine'){
            if($orderData['status'] == 2){
                $order = Orders::where('polishing_status', 2)->first();
                if($order != '') {
                    $order->status = 2;
                    $order->save();
                }
                $getOrderData = Orders::where('polishing_status', 2)->update(['polishing_status' => 5,'polishing_complete' => DB::raw('CURRENT_TIMESTAMP')]);
                Orders::where('id', $orderData['order_id'])->update(['polishing_start' => DB::raw('CURRENT_TIMESTAMP')]);
            }
            $orderStatus = Orders::where('id', $orderData['order_id'])->update(['polishing_status' => $orderData['status']]);
        }
        return $orderStatus;
    }

    public function saveQuantity($data,$user)
    {
        $order_value = Orders::find($data['order_id']);
        $c_quantity = "";
        $ordersQuantity = "";
        if($user->user_type == 'digitalmachine') {
            if($order_value->completed_quantity != ""){
                $c_quantity = ($order_value->completed_quantity+$data['quantity']);
                $ordersQuantity = Orders::where('id',$data['order_id'])->update(['completed_quantity' => $c_quantity]);
            }else{
                $ordersQuantity = Orders::where('id',$data['order_id'])->update(['completed_quantity' => $data['quantity']]);
            }
        }
        if($user->user_type == 'polishingmachine'){
            if($order_value->polishing_completed_quantity != ""){
                $c_quantity = ($order_value->polishing_completed_quantity+$data['quantity']);
                $ordersQuantity = Orders::where('id',$data['order_id'])->update(['polishing_completed_quantity' => $c_quantity]);
            }else{
                $ordersQuantity = Orders::where('id',$data['order_id'])->update(['polishing_completed_quantity' => $data['quantity']]);
            }
        }
        
        $inputData = [];
        $order_value = Orders::find($data['order_id']);
        if($user->user_type == 'digitalmachine') {
            if(($order_value->quantity - $order_value->completed_quantity) != 0){
                $inputData['order_unique_id'] = $order_value->order_unique_id;
                $inputData['user_id'] = $order_value->user_id;
                $inputData['design_number'] = $order_value->design_number;
                
                $inputData['digital_status'] = 3;
                $inputData['quantity'] = ($order_value->quantity - $order_value->completed_quantity);
                $orderCom = Orders::where('id',$data['order_id'])->update(['digital_status' => 5]);
        
                $inputData['note'] = $order_value->note;
                $inputData['admin_status'] = 2;
                $order_create = Orders::create($inputData);
                
                return $order_create;
            }
            else{
                $orderComplete = Orders::where('id',$order_value->id)->update(['digital_status' => 5,'digital_complete' => DB::raw('CURRENT_TIMESTAMP')]);
                return $orderComplete;
            }
        }
        if($user->user_type == 'polishingmachine'){
            if(($order_value->quantity - $order_value->polishing_completed_quantity) != 0){
                $inputData['order_unique_id'] = $order_value->order_unique_id;
                $inputData['user_id'] = $order_value->user_id;
                $inputData['design_number'] = $order_value->design_number;
                
                $inputData['digital_status'] = 5;
                $inputData['polishing_status'] = 3;
                $inputData['quantity'] = ($order_value->completed_quantity != null)?($order_value->completed_quantity - $order_value->polishing_completed_quantity):($order_value->quantity - $order_value->polishing_completed_quantity);

                $orderCom = Orders::where('id',$data['order_id'])->update(['polishing_status' => 5]);

                $inputData['note'] = $order_value->note;
                $inputData['admin_status'] = 2;
                $order_create = Orders::create($inputData);
                
                return $order_create;
            }
            else{
                $orderComplete = Orders::where('id',$order_value->id)->update(['polishing_status' => 5,'polishing_complete' => DB::raw('CURRENT_TIMESTAMP')]);
                return $orderComplete;
            }         
        }
        
    }

    public function deleteOrder($id)
    {
       // Products::where('user_id', $id)->delete();
        $order = Orders::find($id);
        return $order->delete();
    }

}
