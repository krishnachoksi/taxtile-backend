<?php 
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Orders;

class Products extends Model 
{
    use Notifiable;

    use SoftDeletes;

    protected $table = "products";

    protected $guarded = [];
	// protected $fillable = [
	// 'title', 'photo','description','html','css'
	// ];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    public function scopeSearchName($query, $value) {
        return $query->orWhere('users.name', 'LIKE', "%$value%");
    }

    public function scopeSearchOrderId($query, $value) {
        return $query->orWhere('order_id', 'LIKE', "%$value%");
    }

    public function scopeSearchServiceName($query, $value) {
        return $query->orWhere('service_details', 'LIKE', "%$value%");
    }

    public function saveProductData($data)
    {
        if(isset($data['id'])){
            return Products::where('id',$data['id'])->update($data);
        }else{
               foreach ($data['design_number'] as $row)
                {
                    $array[] = [
                        'design_number' => $row,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=> date('Y-m-d H:i:s')
                    ];
                }
                return Products::insert($array);
                // return Products::create($data);
        }
    }

    public function deleteProduct($id)
    {
       // Products::where('user_id', $id)->delete();
        $product = Products::find($id);
        if(isset($product) && $product != ''){
            Orders::where('design_number',$product->design_number)->delete();
        }

        return $product->delete();
    }

}
