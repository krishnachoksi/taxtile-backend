<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Orders;
use App\Products;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];
    protected $guarded = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getCurrentUser() {
        return Auth::user();
    }

    public function setPasswordAttribute($password) {
        $this->attributes['password'] = bcrypt($password);
    }

     /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function saveMemberData($data)
    {
        if(isset($data['id'])){
            $data['password'] = bcrypt($data['password']);
            $update = User::where('id',$data['id'])->update($data);
            return $update;
        }else{
            return User::create($data);
        }
    }

    public function deleteMember($id)
    {
        Orders::where('user_id', $id)->forceDelete();
        $user = User::find($id);
        return $user->forceDelete();
    }

    public function getAllMembers()
    {
        return User::where('user_type', '<>', 'admin')->get();
    }

    public function getAllCount($user)
    {
        $data = [];
        
        if($user->user_type == "admin"){
            $orders = Orders::where('polishing_status','<>',5)->where('digital_status','<>',5)->groupBy('order_unique_id')->get();
            $data['orders'] = $orders->count();
            $data['users'] = User::where('user_type', '<>', 'admin')->count();
            $data['product'] = Products::count();
        }
        else if($user->user_type == "digitalmachine"){
            $data['orders'] = Orders::where('digital_status',2)->count();
        }
        else if($user->user_type == "polishingmachine"){
            $data['orders'] = Orders::where('polishing_status',2)->count();
        }
        else{
            $data['orders'] = Orders::where('user_id',$user->id)->count();
        }
        return $data;
    }
}
